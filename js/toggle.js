function toggleFunctionTemp() {
    var x = document.getElementById("tempList");
    if (x.style.display === "none") {
      localStorage.setItem('displayTemp','block')
        x.style.display = "block";
        $('#tempButton').html("Hide Temp");
    } else {
        localStorage.setItem('displayTemp','none')
        x.style.display = "none";
        $('#tempButton').html("Show Temp");
    }
  console.log(localStorage)
}

function checkTempDisplay(){
      var x = document.getElementById("tempList");
      x.style.display = localStorage.getItem('displayTemp');
}


function toggleFunctionIcon() {
    var x = document.getElementById("iconList");
    if (x.style.display === "none") {
      localStorage.setItem('displayIcon','block')
        x.style.display = "block";
        $('#iconButton').html("Hide Icon");
    } else {
        localStorage.setItem('displayIcon','none')
        x.style.display = "none";
        $('#iconButton').html("Show Icon");
    }
  console.log(localStorage)
}

function checkIconDisplay(){
      var x = document.getElementById("iconList");
      x.style.display = localStorage.getItem('displayIcon');
}



function toggleFunctionHumidity() {
    var x = document.getElementById("humList");
    if (x.style.display === "none") {
      localStorage.setItem('displayHum','block')
        x.style.display = "block";
        $('#humButton').html("Hide Humidity");
    } else {
        localStorage.setItem('displayHum','none')
        x.style.display = "none";
        $('#humButton').html("Show Humidity");
    }
  console.log(localStorage)
}

function checkHumidityDisplay(){
      var x = document.getElementById("humList");
      x.style.display = localStorage.getItem('displayHum');
}


function toggleFunctionDescription() {
    var x = document.getElementById("descList");
    if (x.style.display === "none") {
      localStorage.setItem('displayDesc','block')
        x.style.display = "block";
        $('#descButton').html("Hide Description");
    } else {
        localStorage.setItem('displayDesc','none')
        x.style.display = "none";
        $('#descButton').html("Show Description");
    }
  console.log(localStorage)
}

function checkDescriptionDisplay(){
      var x = document.getElementById("descList");
      x.style.display = localStorage.getItem('displayDesc');
}


function toggleFunctionHistory() {
    var x = document.getElementById("historyList");
    if (x.style.display === "none") {
      localStorage.setItem('displayHist','block')
        x.style.display = "block";
        $('#histButton').html("Hide History");
    } else {
        localStorage.setItem('displayHist','none')
        x.style.display = "none";
        $('#histButton').html("Show History");
    }
  console.log(localStorage)
}

function checkHistoryDisplay(){
      var x = document.getElementById("historyList");
      x.style.display = localStorage.getItem('displayHist');
}




