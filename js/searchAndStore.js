//---------------------- show and hide pages ----------------------//



//---------------------- location data ----------------------//


// location by geolocation

    if (!Modernizr.geolocation){
        alert('Geolocation not supported')
    }else{
        function geoFindMe() {
            var output = document.getElementById("out");

            if (!navigator.geolocation){
            output.innerHTML = "<p>Geolocation is not supported by your browser</p>";
            return;
            }

            function success(position) {
            var latitude  = position.coords.latitude;
            var longitude = position.coords.longitude;

            output.innerHTML = '<p>Latitude is ' + latitude + '° <br>Longitude is ' + longitude + '°</p>';

            var img = new Image();
            img.src = "https://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=13&size=300x300&sensor=false";

            output.appendChild(img);


            $.ajax({
                //temp displayed in celsius as defined as metric - can improve by adding user defined value
                url: "https://api.openweathermap.org/data/2.5/forecast?lat=" + latitude + "&lon=" + longitude + "&units=metric" + "&APPID=95fc39038592d97f28c0c68203dcd890&cnt=40",
                type: "GET",
                dataType: "jsonp",
                success: function(forecastData){
                    var forecastWeather = showForecast(forecastData);
                }
            });


            $.ajax({
                //temp displayed in celsius as defined as metric - can improve by adding user defined value
                url: "https://api.openweathermap.org/data/2.5/weather?lat=" + latitude + "&lon=" + longitude + "&units=metric" + "&APPID=95fc39038592d97f28c0c68203dcd890&cnt=40",
                type: "GET",
                dataType: "jsonp",
                success: function(weatherData){
                    // console.log(weatherData);
                    var currentWeather = showCurrent(weatherData);
                    // $("#show").html(information);
                }
            });
}

            function error() {
            output.innerHTML = "Unable to retrieve your location";
            }

            output.innerHTML = "<p>Locating…</p>";

            navigator.geolocation.getCurrentPosition(success, error);
        }
    }





// location by textual search

$(document).ready(function(){


    $('.submitLocation').click(function(){

        //get value from input field
        var city = $(".city").val();

        //check not empty
        if (city !== ''){

            $.ajax({
                //temp displayed in celsius as defined as metric - can improve by adding user defined value
                url: "https://api.openweathermap.org/data/2.5/forecast?q=" + city + "&units=metric" + "&APPID=95fc39038592d97f28c0c68203dcd890&cnt=40",
                type: "GET",
                dataType: "jsonp",
                success: function(forecastData){
                    var forecastWeather = showForecast(forecastData);
                }
            });

            $.ajax({
                //temp displayed in celsius as defined as metric - can improve by adding user defined value
                url: "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric" + "&APPID=95fc39038592d97f28c0c68203dcd890&cnt=40",
                type: "GET",
                dataType: "jsonp",
                success: function(weatherData){
                    var currentWeather = showCurrent(weatherData);
                }
            });

        }else{
            $('#error').html('Field cannot be empty');
        }

    });
});




//---------------------- current data ----------------------//


var weather;
var weatherLength;
var text;
var i;


function showCurrent(currentWeather) {

    console.log(currentWeather);

    // check existing storage to ensure array is never overwritten
    if (localStorage.getItem("weather") === null){
        weather = []
    }else{
        weather = JSON.parse(localStorage.getItem("weather"));
    }

    // for the forloop - adds new name
    weather.push( currentWeather.name );

    if (typeof(Storage) !== "undefined") {

        // Store
        //Session Storage
        //saves name/s in local storage (for histoy list)
        localStorage.setItem("weather", JSON.stringify( weather ) );

        //Local Storage
        //saves name in session storage for current use
        sessionStorage.setItem("name", currentWeather.name);
        sessionStorage.setItem("temp", currentWeather.main.temp);
        sessionStorage.setItem("min", currentWeather.main.temp_min);
        sessionStorage.setItem("max", currentWeather.main.temp_max);
        sessionStorage.setItem("pres", currentWeather.main.pressure);
        sessionStorage.setItem("hum", currentWeather.main.humidity);
        sessionStorage.setItem("desc", currentWeather.weather[0].main);
        sessionStorage.setItem("icon", currentWeather.weather[0].icon);
        

        // Retrieve
        // Retrieves name from session storage 
        document.getElementById("name").innerHTML = sessionStorage.getItem("name");

        // Retrieves list of previous locations
        weatherLength = weather.length;
        text = "<ul>";
        for (i = 0; i < weatherLength; i++) {
            text += "<li class='city'>" + weather[i] + "</li>";
        }
        text += "</ul>";
        
        //Outputs list
        document.getElementById("historyList").innerHTML = text;

        //Outputs result as string
        // var storedLocations = document.getElementById("historyList2").innerHTML = JSON.parse(localStorage.getItem("weather"));


  }
    else {
        document.getElementById("error").innerHTML = "Sorry, your browser does not support Web Storage...";
    }

}



//---------------------- forecast data ----------------------//

var temp;
var icon;
var desc;
var windDeg;
var windSp;
var seaLv;
var press;
var hum;
var gdLvl;
var day;

function showForecast(forecastWeather) {

    if (typeof(Storage) !== "undefined") {

        //defined empty arrays here to ensure content is replaced with new .push content
        var temp = [];
        var icon = [];
        var desc = [];
        var windDeg = [];
        var windSp = [];
        var seaLv = [];
        var press = [];
        var hum = [];
        var gdLvl = [];
        var day = [];

        // gets results which are noon time
        var a = forecastWeather.list;
        for (index = 0; index < a.length; ++index) {
        
            var forecastTemp = a[index].main.temp;
            var forecastIcon = a[index].weather[0].icon;
            var forecastDesc = a[index].weather[0].main;
            var forecastWindDegree = a[index].wind.deg;
            var forecastWindSpeed = a[index].wind.speed;
            var forecastSeaLevel = a[index].main.sea_level;
            var forecastPressure = a[index].main.pressure;
            var forecastHumidity = a[index].main.humidity;
            var forecastGroundLevel = a[index].main.grnd_level;
            var forecastDay = a[index].dt_txt;
            
            sessionStorage.setItem("forecastTemp", JSON.stringify( temp ) );
            sessionStorage.setItem("forecastIcon", JSON.stringify( icon ) );
            sessionStorage.setItem("forecastDesc", JSON.stringify( desc ) );
            sessionStorage.setItem("forecastWindDegree", JSON.stringify( windDeg ) );
            sessionStorage.setItem("forecastWindSpeed", JSON.stringify( windSp ) );
            sessionStorage.setItem("forecastSeaLevel", JSON.stringify( seaLv ) );
            sessionStorage.setItem("forecastPressure", JSON.stringify( press ) );
            sessionStorage.setItem("forecastHumidity", JSON.stringify( hum ) );
            sessionStorage.setItem("forecastGroundLevel", JSON.stringify( gdLvl ) );
            sessionStorage.setItem("forecastDay", JSON.stringify( day ) );

            //get utc
            var str = a[index].dt_txt;
            console.log(str);

            // target 12 noon
            var time = str.substring(11, 13);

            // get dd/mm/yy details
            var days = str.substring(8, 10);
            var month = str.substring(5, 7);
            var year = str.substring(0, 4);

            //get only 12 noon details
            if (time == 12){
               // console.log(str); 
               temp.push( forecastTemp );
               icon.push( forecastIcon );
               desc.push( forecastDesc );
               windDeg.push( forecastWindDegree );
               windSp.push( forecastWindSpeed );
               seaLv.push( forecastSeaLevel );
               press.push( forecastPressure );
               hum.push( forecastHumidity );
               gdLvl.push( forecastGroundLevel );
               day.push( forecastDay );

            }
        }
    }
    else {
        document.getElementById("error").innerHTML = "Sorry, your browser does not support Web Storage...";
    }


}

//put here to display name on return to search page and on every other page
document.getElementById("name").innerHTML = sessionStorage.getItem("name");
