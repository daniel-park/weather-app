var nameEl;
var tempEl;
var maxEl;
var minEl;
var descEl;
var iconEl;
var iconUrl;

function populateContent(weather)
{

    nameEl=document.getElementById("name").innerHTML = sessionStorage.getItem("name");
    tempEl=document.getElementById("temp").innerHTML = sessionStorage.getItem("temp");
    minEl=document.getElementById("min").innerHTML = sessionStorage.getItem("min");
    maxEl=document.getElementById("max").innerHTML = sessionStorage.getItem("max");
    descEl=document.getElementById("desc").innerHTML = sessionStorage.getItem("desc");
    iconEl=document.getElementById("icon").innerHTML = sessionStorage.getItem("icon");

    nameEl.innerHTML = weather.name;
    tempEl.innerHTML = weather.temp;
    minEl.innerHTML = weather.min;
    maxEl.innerHTML = weather.max;
    descEl.innerHTML = weather.desc;
    iconEl.innerHTML = weather.icon;

    iconUrl = "icons/" + iconEl + ".png";
    $('#icon').attr('src', iconUrl);

    // console.log(nameEl);
    // console.log(tempEl);
    // console.log(minEl);
    // console.log(maxEl);
    // console.log(descEl);
    // console.log(iconEl);
    // console.log(iconUrl);

    if(nameEl.innerHTML === "undefined"){
        nameEl.remove();
    }
    if(tempEl.innerHTML === "undefined"){
        tempEl.remove();
    }
    if(minEl.innerHTML === "undefined"){
        minEl.remove();
    }
    if(maxEl.innerHTML === "undefined"){
        maxEl.remove();
    }
    if(descEl.innerHTML === "undefined"){
        descEl.remove();
    }
    if(iconEl.innerHTML === "undefined"){
        iconEl.remove();
    }


}



$(document).ready(function() {
  $(".animsition").animsition({
    inClass: 'fade-in-up',
    outClass: 'fade-out-up',
    inDuration: 1500,
    outDuration: 800,
    linkElement: '.animsition-link',
    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
    loading: true,
    loadingParentElement: 'body', //animsition wrapper element
    loadingClass: 'animsition-loading',
    loadingInner: '', // e.g '<img src="loading.svg" />'
    timeout: false,
    timeoutCountdown: 5000,
    onLoadEvent: true,
    browser: [ 'animation-duration', '-webkit-animation-duration'],
    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
    overlay : false,
    overlayClass : 'animsition-overlay-slide',
    overlayParentElement : 'body',
    transition: function(url){ window.location.href = url; }
  });
});


// initialise settings menu plugin
    $(document).ready(function() {
        $('.menu-link').bigSlide();
    });


function init(){
    var weather = sessionStorage.getItem("name");
    populateContent(weather);
}


init();