var weather;
var weatherLength;
var text;
var i;



// check if array is empty to prevent emptying it on further searches

if (localStorage.getItem("weather") === null){
    weather = []
}else{
    weather = JSON.parse(localStorage.getItem("weather"));
}

// Retrieves list of previous locations
weatherLength = weather.length;
text = "<ul id='history'>";
for (i = 0; i < weatherLength; i++) {
    text += "<li class='city'>" + weather[i] + "</li>";
}
text += "</ul>";

//Outputs list
document.getElementById("historyList").innerHTML = text;




$(document).ready(function() {
  $(".animsition").animsition({
    inClass: 'fade-in-up',
    outClass: 'fade-out-up',
    inDuration: 1500,
    outDuration: 800,
    linkElement: '.animsition-link',
    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
    loading: true,
    loadingParentElement: 'body', //animsition wrapper element
    loadingClass: 'animsition-loading',
    loadingInner: '', // e.g '<img src="loading.svg" />'
    timeout: false,
    timeoutCountdown: 5000,
    onLoadEvent: true,
    browser: [ 'animation-duration', '-webkit-animation-duration'],
    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
    overlay : false,
    overlayClass : 'animsition-overlay-slide',
    overlayParentElement : 'body',
    transition: function(url){ window.location.href = url; }
  });
});


// initialise settings menu plugin
    $(document).ready(function() {
        $('.menu-link').bigSlide();
    });


