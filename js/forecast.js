var forecastTempEl;
var forecastIconEl;

var forecastDescEl;
var forecastWindDegreeEl;
var forecastWindSpeedEl;
var forecastSeaLevelEl;
var forecastPressureEl;
var forecastHumidityEl;
var forecastGroundLevelEl;
var forecastDayEl;


function populateContent(forecast)
{

    forecastTempEl = sessionStorage.getItem("forecastTemp");
    forecastIconEl= sessionStorage.getItem("forecastIcon");

    forecastDescEl= sessionStorage.getItem("forecastDesc");
    forecastWindDegreeEl= sessionStorage.getItem("forecastWindDegree");
    forecastWindSpeedEl= sessionStorage.getItem("forecastWindSpeed");
    forecastSeaLevelEl= sessionStorage.getItem("forecastSeaLevel");
    forecastPressureEl= sessionStorage.getItem("forecastPressure");
    forecastHumidityEl= sessionStorage.getItem("forecastHumidity");
    forecastGroundLevelEl= sessionStorage.getItem("forecastGroundLevel");
    forecastDayEl= sessionStorage.getItem("forecastDay");

    var objTemp = JSON.parse(forecastTempEl);
    var objIcon = JSON.parse(forecastIconEl);
    var objHum = JSON.parse(forecastHumidityEl);
    var objDesc = JSON.parse(forecastDescEl);
    // var objDay = JSON.parse(forecastDayEl);



    // loop through forecast results

    var datesLength;
    var datesText;
    var dates;

        datesLength = objTemp.length;
        datesText = "<div id='dateList'>";
        for (dates = 0; dates < datesLength; dates++) {

            //change display of forecastDayEl (date) to dd/mm/yyyy
            // source begins: stackoverflow
            const dateInput = JSON.parse(forecastDayEl);
            const date = dateInput.map((dateStr) => {
              const [year, month, day] = dateStr.match(/\d+/g);
              return [day, month, year].join('-');
            });
            // source ends

            datesText += "<ul class='results'>" + "<li class='forecastDate'>" + date[dates] + "</li>" + "</ul>";
        }
        datesText += "</div>";

        //outputs result as list
        document.getElementById("dateList").innerHTML = datesText;




    var tempLength;
    var tempText;
    var temp;

        tempLength = objTemp.length;
        tempText = "<div class='tempList'>";
        for (temp = 0; temp < tempLength; temp++) {

            //change display of forecastDayEl (date) to dd/mm/yyyy
            // source begins: stackoverflow
            const dateInput = JSON.parse(forecastDayEl);
            const date = dateInput.map((dateStr) => {
              const [year, month, day] = dateStr.match(/\d+/g);
              return [day, month, year].join('-');
            });
            // source ends

            tempText += "<ul class='results'>" + "<li class='forecastTemperature'>" + objTemp[temp] + "</li>" + "</ul>";
        }
        tempText += "</div>";

        //outputs result as list
        document.getElementById("tempList").innerHTML = tempText;


    var iconLength;
    var iconText;
    var icon;

        iconLength = objIcon.length;
        iconText = "<div class='iconList'>";
        for (icon = 0; icon < iconLength; icon++) {

            //change display of forecastDayEl (date) to dd/mm/yyyy
            // source begins: stackoverflow
            const dateInput = JSON.parse(forecastDayEl);
            const date = dateInput.map((dateStr) => {
              const [year, month, day] = dateStr.match(/\d+/g);
              return [day, month, year].join('-');
            });
            // source ends

            iconText += "<ul class='results'>" + "<li class='forecastIcon'>" + "<img id='icon'" + "src='icons/" + objIcon[icon] + ".png'" + "alt='Weather icon'>" + "</li>" + "</ul>";
        }
        iconText += "</div>";

        //outputs result as list
        document.getElementById("iconList").innerHTML = iconText;


    var humLength;
    var humText;
    var hum;

        humLength = objHum.length;
        humText = "<div class='humList'>";
        for (hum = 0; hum < humLength; hum++) {

            //change display of forecastDayEl (date) to dd/mm/yyyy
            // source begins: stackoverflow
            const dateInput = JSON.parse(forecastDayEl);
            const date = dateInput.map((dateStr) => {
              const [year, month, day] = dateStr.match(/\d+/g);
              return [day, month, year].join('-');
            });
            // source ends

            humText += "<ul class='results'>" + "<li class='forecastHumidity'>" + objHum[hum] + "</li>" + "</ul>";
        }
        humText += "</div>";

        //outputs result as list
        document.getElementById("humList").innerHTML = humText;


    var descLength;
    var descText;
    var desc;

        descLength = objDesc.length;
        descText = "<div class='descList'>";
        for (desc = 0; desc < descLength; desc++) {

            //change display of forecastDayEl (date) to dd/mm/yyyy
            // source begins: stackoverflow
            const dateInput = JSON.parse(forecastDayEl);
            const date = dateInput.map((dateStr) => {
              const [year, month, day] = dateStr.match(/\d+/g);
              return [day, month, year].join('-');
            });
            // source ends

            descText += "<ul class='results " + objDesc[desc] + "'>" + "<li class='forecastDesc'>" + objDesc[desc] + "</li>" + "</ul>";
        }
        descText += "</div>";

        //outputs result as list
        document.getElementById("descList").innerHTML = descText;

}






$(document).ready(function() {
  $(".animsition").animsition({
    inClass: 'fade-in-up',
    outClass: 'fade-out-up',
    inDuration: 1500,
    outDuration: 800,
    linkElement: '.animsition-link',
    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
    loading: true,
    loadingParentElement: 'body', //animsition wrapper element
    loadingClass: 'animsition-loading',
    loadingInner: '', // e.g '<img src="loading.svg" />'
    timeout: false,
    timeoutCountdown: 5000,
    onLoadEvent: true,
    browser: [ 'animation-duration', '-webkit-animation-duration'],
    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
    overlay : false,
    overlayClass : 'animsition-overlay-slide',
    overlayParentElement : 'body',
    transition: function(url){ window.location.href = url; }
  });
});


// initialise settings menu plugin
    $(document).ready(function() {
        $('.menu-link').bigSlide();
    });


function init(){
    var forecast = sessionStorage.getItem("name");
    populateContent(forecast);
}


init();




